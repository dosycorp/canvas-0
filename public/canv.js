  const ctx = canvas.getContext('2d');
  let drawing;

  img.onload = start;

  button.onclick = () => {
    if ( file.files && file.files[0] ) {
      const reader = new FileReader();
      reader.onload = () => {
        clearInterval(drawing);
        drawing = null;
        img.onload = start;
        img.src = reader.result;
      }
      reader.readAsDataURL(file.files[0]);
    }
  };
  stopstart.onclick = () => {
    if ( !! drawing ) {
      clearInterval(drawing);
      drawing = null;
    } else {
      drawing = setInterval(draw,40);
    }
  }

  img.src = "disc.gif" ;

  function ran() {
    const val = Math.random();
    if ( val > 0.67 ) return offset.value;
    else if ( val < 0.33 ) return -offset.value;
    else return 0;
  }

  function start() {
    canvas.width = Math.min(800,Math.max(300,img.width));
    canvas.height = Math.min(600,Math.max(150,img.height));
    img.onload = null;
    drawing = setInterval(draw,40);
  }

  function draw() {
    ctx.drawImage(img, ran(), ran(), canvas.width, canvas.height);  
    img.src = canvas.toDataURL('image/' + mime.value, 0.5);
  }

